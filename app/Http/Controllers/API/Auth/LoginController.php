<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Session;
use Exception;
use Session;


class LoginController extends Controller
{
    
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        if ($validator->fails()) {
            return apiResponseValidationFails('Login validation fails!', $validator->errors()->all(), 422);
        }

        if (Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
            
        ])) {
            $user = Auth::user();
            $success['user'] = $user;
            $success['token'] = $user->createToken('myApp')->accessToken;

            Session::put('user_id',$user->id);

            return apiResponseSuccess('Anda berhasil login!', $success, 200);
        } else {
            return apiResponseErrors('Gagal login!', [
                'User belum terdaftar atau password anda salah'
            ], 401);
        }

    }
  
}
